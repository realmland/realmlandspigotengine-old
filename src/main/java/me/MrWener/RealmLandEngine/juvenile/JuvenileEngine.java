package me.MrWener.RealmLandEngine.juvenile;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import me.MrWener.RealmLandEngine.juvenile.model.RealmLandJuvenile;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JuvenileEngine implements PluginComponent {

    private RealmLandEngine engine;

    // Kids come here hehe
    private List<RealmLandJuvenile> juveniles = new ArrayList<>();

    public JuvenileEngine(@NotNull RealmLandEngine engine) {
        this.engine = engine;
    }

    /**
     * Registers all RealmLand "juveniles"
     *
     * @param kid   Kid
     * @param <Kid> Type of Kid
     */
    public <Kid extends RealmLandJuvenile> void registerJuvenile(@NotNull Kid kid) {
        juveniles.add(kid);
    }

    @Override
    public void initialize() {
        findJuveniles();
    }

    @Override
    public void terminate() {

    }

    /**
     * Searches for all Juveniles
     */
    public void findJuveniles() {
        Arrays.asList(Bukkit.getPluginManager().getPlugins()).stream().forEach(plugin -> {
            if (!plugin.getName().equalsIgnoreCase(engine.getName())) {
                if (Arrays.asList(plugin.getClass().getInterfaces()).contains(RealmLandJuvenile.class)) {
                    registerJuvenile((RealmLandJuvenile) plugin);
                }
            }
        });
    }

    /**
     * Checks if Juvenile is registered
     *
     * @param kid   Juvenile
     * @param <Kid> Type of Juvenile
     * @return true/false
     */
    public <Kid extends RealmLandJuvenile> boolean isJuvenileRegistered(@NotNull Kid kid) {
        return juveniles.contains(kid);
    }

    /**
     * @return List of all juveniles
     */
    public List<RealmLandJuvenile> getJuveniles() {
        return juveniles;
    }
}
