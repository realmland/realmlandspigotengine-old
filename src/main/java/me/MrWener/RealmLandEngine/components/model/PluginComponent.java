package me.MrWener.RealmLandEngine.components.model;

public interface PluginComponent {

    void initialize();
    void terminate();

}
