package me.MrWener.RealmLandEngine.nbt.entity;

import net.minecraft.server.v1_13_R2.NBTBase;
import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.CraftServer;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

@Deprecated
public class NBTEntityEditor {

    /**
     * Writes {@param <Data>} value into NBT of entity
     *
     * @param entity Entity
     * @param key    Key of NBTTag
     * @param value  Value of NBTTag
     * @param <Data> Type of NBTTag value data
     * @return Edited entity
     */
    public static <Data extends NBTBase> Entity writeNBT(@NotNull Entity entity, @NotNull String key, @NotNull Data value) {
        NBTTagCompound compound = new NBTTagCompound();
        compound.set(key, value);

        return saveEntityNBT(entity, compound);
    }

    /**
     * Writes Array into NBT of Entity
     *
     * @param entity Entity
     * @param key    Key of Array
     * @param value  Array of values
     * @param <Data> Type of Data
     * @return
     */
    public static <Data extends NBTBase> Entity writeArrayNBT(@NotNull Entity entity, @NotNull String key, @NotNull Collection<Data> value) {
        NBTTagList list = new NBTTagList();
        value.forEach(entry -> list.add(entry));

        return writeNBT(entity, key, list);
    }

    /**
     * Reads NBTTag of Entity
     *
     * @param entity Entity
     * @param key    Key of NBTTag
     * @return
     */
    public static NBTBase readNBT(@NotNull Entity entity, @NotNull String key) {
        NBTTagCompound compound = getEntityNBT(entity);
        NBTBase val = compound.get(key);

        return val;
    }

    /**
     * Reads NBTArray of Entity NBTCompound
     *
     * @param entity Entity
     * @param key    Key
     * @return Collection
     */
    public static Collection<NBTBase> readArrayNBT(@NotNull Entity entity, @NotNull String key) {
        Collection<NBTBase> collection = new ArrayList<>();
        NBTTagCompound compound = getEntityNBT(entity);

        NBTTagList list = (NBTTagList) compound.get(key);
        collection.addAll(list);
        return collection;
    }

    /**
     * Checks if NBTCompound has tag
     *
     * @param entity Entity
     * @param key    Key
     * @return True/False
     */
    public static boolean hasTag(@NotNull Entity entity, @NotNull String key) {
        return getEntityNBT(entity).hasKey(key);
    }

    /**
     * @param entity Entity
     * @return Entity's NBTTagCompound
     */
    public static NBTTagCompound getEntityNBT(@NotNull Entity entity) {
        net.minecraft.server.v1_13_R2.Entity nmsEntity = ((CraftEntity) entity).getHandle();
        return nmsEntity.save(new NBTTagCompound());
    }

    /**
     * Saves entity NBTTagCompound
     *
     * @param entity   Entity
     * @param compound New Compound
     * @return Edited Entity
     */
    public static Entity saveEntityNBT(@NotNull Entity entity, @NotNull NBTTagCompound compound) {
        net.minecraft.server.v1_13_R2.Entity nmsEntity = ((CraftEntity) entity).getHandle();
        nmsEntity.save(compound);

        return CraftEntity.getEntity(((CraftServer) Bukkit.getServer()), nmsEntity);
    }

}
