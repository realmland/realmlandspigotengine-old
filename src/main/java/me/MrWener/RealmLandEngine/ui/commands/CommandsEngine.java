package me.MrWener.RealmLandEngine.ui.commands;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import me.MrWener.RealmLandEngine.exceptions.InvalidCommandException;
import me.MrWener.RealmLandEngine.localization.Localization;
import me.MrWener.RealmLandEngine.localization.chat.TextComponentBuilder;
import me.MrWener.RealmLandEngine.logging.Logger;
import me.MrWener.RealmLandEngine.ui.commands.model.RealmLandCommand;
import me.MrWener.RealmLandEngine.ui.commands.model.RealmLandSubCommand;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.util.*;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class CommandsEngine implements PluginComponent {


    // Instance
    private RealmLandEngine instance;

    // Map
    private CommandMap commandMap;

    private Map<String, RealmLandCommand> commands = new HashMap<>();

    // Logger obviously
    private Logger logger;

    /**
     * Default constructor
     *
     * @param instance
     */
    public CommandsEngine(@NotNull RealmLandEngine instance) {
        this.instance = instance;
        this.logger = instance.logger();

        this.commandMap = ((CraftServer) instance.getServer()).getCommandMap();
    }

    @Override
    public void initialize() {
        findCommands(instance);

        instance.getJuvenileEngine().getJuveniles().stream().forEach(juvenile -> {
            if (juvenile instanceof JavaPlugin) {
                findCommands((JavaPlugin) juvenile);
            }
        });

    }

    @Override
    public void terminate() {

    }

    /**
     * Registers {@link Command} into command map
     *
     * @param command   Command
     * @param <Command> Type of Command
     * @return true, false
     */
    public <Command extends RealmLandCommand> boolean registerCommand(@NotNull Command command) {
        try {
            Constructor constructor = command.getClass().getConstructor(RealmLandEngine.class);
            Annotation data = constructor.getDeclaredAnnotation(CommandData.class);
            if (data == null) {
                instance.logger().err("", new InvalidCommandException(command.getClass().getSimpleName(),
                        InvalidCommandException.Type.MISSING_ANNOTATION));
                return false;
            }

            String commandName = ((CommandData) data).name();
            String[] commandAliases = ((CommandData) data).aliases();
            String permissionString = ((CommandData) data).permission();


            if (!instance.getPermissionsHandler().permissionExists(permissionString))
                logger.warn("Command '" + commandName + "' has permission ('" + permissionString + "') that was not registered yet!");


            org.bukkit.command.Command bukkitCommand = new org.bukkit.command.Command(commandName, "", "", Arrays.asList(commandAliases)) {
                @Override
                public boolean execute(CommandSender sender, String label, String[] args) {
                    if (!sender.hasPermission(permissionString)) {
                        sender.sendMessage(instance.getPermissionsHandler().getPermissionMessage());
                        return true;
                    }

                    // check subcommands
                    if (args.length > 0) {
                        String subcommand = args[0];

                        Iterator<RealmLandSubCommand> subCommands = command.getSubCommands().values().iterator();
                        while (subCommands.hasNext()) {
                            RealmLandSubCommand sub = subCommands.next();
                            // if using subcommand
                            if (subcommand.equalsIgnoreCase(sub.getName())) {
                                // if action is set
                                if (args.length > 1) {
                                    String action = args[1].toLowerCase();
                                    String[] actionArgs = Arrays.copyOfRange(args, 2, args.length);


                                    // if action is 'actions'
                                    if (action.equalsIgnoreCase("actions")) {
                                        // todo better UI
                                        sender.spigot().sendMessage(
                                                new TextComponentBuilder(
                                                        Localization.PREFIX_INFO, "List of available actions: \n   §7- "
                                                        + sub.getActions().stream().map(RealmLandSubCommand.Action::getName).collect(Collectors.joining(", "))).getComponent());
                                        return true;
                                    }

                                    // if action exists
                                    if (sub.containsAction(action)) {
                                        return sub.getAction(action).executeAction(sender, label, actionArgs);
                                    }
                                }
                                return sub.executeSubCommand(sender, label, args);
                            }
                        }
                    }
                    // just execute command
                    return command.executeCommand(sender, label, args);
                }

                @Override
                public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws
                        IllegalArgumentException {
                    if (args.length == 1) {
                        return command.getSubCommands().values().stream().map(cmd -> cmd.getName()).collect(Collectors.toList());
                    } else if (args.length > 1) {
                        String subcommand = args[0];
                        Iterator<RealmLandSubCommand> subCommandListIterator = command.getSubCommands().values().iterator();

                        RealmLandSubCommand sub = null;
                        while (subCommandListIterator.hasNext()) {
                            RealmLandSubCommand next = subCommandListIterator.next();
                            if (next.getName().equalsIgnoreCase(subcommand)) {
                                sub = next;
                            }
                        }

                        if (args.length > 2) {
                            String actionName = args[1];
                            List<String> requirements = new ArrayList<>();
                            if (sub != null) {
                                /*ListIterator<RealmLandSubCommand.Action> actionListIterator = sub.getActions().listIterator();
                                while (actionListIterator.hasNext()) {
                                    RealmLandSubCommand.Action action = actionListIterator.next();
                                    if(action.getName().equalsIgnoreCase(actionName)) {
                                        requirements = action.getRequirements();
                                        System.out.println("assign");
                                    }
                                }

                                if(requirements.get(args.length - 1) != null) {
                                    return Arrays.asList(requirements.get(args.length));
                                }
                            */
                            }
                            return new ArrayList<>();
                        }


                        if (sub.getName().equalsIgnoreCase(subcommand)) {
                            List<String> actions = sub.getActions().stream().map(action -> action.getName()).collect(Collectors.toList());
                            actions.add("actions");
                            return actions;
                        }
                        return new ArrayList<>();
                    }
                    return new ArrayList<>();
                }
            };
            bukkitCommand.setPermission(permissionString);
            commands.put(commandName, command);
            return commandMap.register("fallback_" + commandName, bukkitCommand);
        } catch (NoSuchMethodException e) {
            logger.err("Caught exception while trying to register command", new InvalidCommandException(command.getClass().getSimpleName(),
                    InvalidCommandException.Type.MISSING_CONSTRUCTOR));
        }
        return false;
    }

    /**
     * Searches for classes that are instance of {@link RealmLandCommand} and then registers them
     *
     * @param plugin Plugin
     */
    public <Plugin extends JavaPlugin> void findCommands(@NotNull Plugin plugin) {
        try {
            new JarFile(new File(plugin.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()))
                    .stream()
                    .map(entry -> entry.getName())
                    .filter(name -> name.endsWith(".class"))
                    .map(name -> name.substring(0, name.length() - ".class".length()))
                    .map(name -> name.replaceAll("/", "."))
                    .map(name -> {
                        try {
                            return Class.forName(name);
                        } catch (Throwable ignored) {
                        }
                        return null;
                    })
                    .filter(clazz -> clazz != null)
                    .filter(clazz -> clazz.getSuperclass() != null)
                    .filter(clazz -> clazz.getSuperclass().equals(RealmLandCommand.class))
                    .map(clazz -> {
                        try {
                            return clazz.getConstructor(RealmLandEngine.class).newInstance(instance);
                        } catch (Throwable ignored) {
                        }
                        return null;
                    })
                    .filter(clazz -> clazz != null)
                    .map(clazz -> (RealmLandCommand) clazz)
                    .forEach(clazz -> registerCommand(clazz));

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tries to find command by name
     *
     * @param name Name of command
     * @return RealmLandCommand
     */
    public RealmLandCommand getCommand(@NotNull String name) {
        return getCommands().get(name);
    }

    /**
     * @return All commands
     */
    public Map<String, RealmLandCommand> getCommands() {
        return commands;
    }
}
