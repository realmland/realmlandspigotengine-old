package me.MrWener.RealmLandEngine.localization;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import me.MrWener.RealmLandEngine.database.DatabaseEngine;
import me.MrWener.RealmLandEngine.database.payload.Payload;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Localization implements PluginComponent {

    private RealmLandEngine instance;
    private @NotNull
    DatabaseEngine database;

    private HashMap<String, Payload> messages = new HashMap<>();


    public static final String PREFIX_ERR = "§c# §7";
    public static final String PREFIX_SUCCESS = "§a# §7";
    public static final String PREFIX_WARN = "§e# §7";
    public static final String PREFIX_INFO = "§9# §7";


    public Localization(@NotNull RealmLandEngine instance) {
        this.instance = instance;
        this.database = instance.getDatabase();

        database.executeSQL("CREATE TABLE IF NOT EXISTS `localization` (" +
                "  `message_key` VARCHAR(128) NOT NULL," +
                "  `sk` TEXT NULL," +
                "  `cz` TEXT NULL," +
                "  `en` TEXT NULL," +
                "  PRIMARY KEY (`message_key`)" +
                ")");

    }

    @Override
    public void initialize() {
        List<Payload> query = database.SQLSelect("localization", Arrays.asList("message_key", "sk", "cz", "en"), null);
        query.forEach(message -> {
            messages.put(message.getColumn("message_key"), message);
        });


    }

    @Override
    public void terminate() {
        messages.clear();
    }

    /**
     * @param key  Message key
     * @param lang Message language
     * @return TextComponent
     */
    public TextComponent getMessage(@NotNull String key, @NotNull Lang lang) {
        String message = "";

        if (messages.get(key) == null)
            message = null;
        else
            message = messages.get(key).getColumn(lang.name().toLowerCase());

        if (message == null) {
            TextComponent err = new TextComponent(Localization.PREFIX_ERR + "Failed to retrieve message with key '" + key + "' for lang '" + lang.name() + "'");
            err.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                    "/engine localization set " + key + " " + lang.name().toLowerCase() + " message"));

            return err;
        }

        return new TextComponent(ComponentSerializer.parse(message));
    }

    /**
     * Adds message into void or whatever
     * @param key    Key of message
     * @param message Message itself
     * @param lang    Lang
     * @return Success/Failure
     */
    public boolean addMessage(@NotNull String key, @NotNull TextComponent message, @NotNull Lang lang) {
        if (messages.containsKey(key))
            return false;
        else
            setMessage(key, message, lang);
        return true;
    }

    /**
     * Adds message to plugin stored messages and in database.
     * Replaces old values
     *
     * @param key     Message key
     * @param message Message
     * @param lang    Language
     */
    public void setMessage(@NotNull String key, @NotNull TextComponent message, @NotNull Lang lang) {
        Payload newMessage = new Payload();

        // get already defined payload if exists, and rewrite language message
        if (messages.containsKey(key)) {
            newMessage = messages.get(key);
        }

        newMessage.forceAddColumn(lang.name().toLowerCase(), ComponentSerializer.toString(message));
        // add finally
        messages.put(key, newMessage);

        // if database contains message for this lang, update! yeeey!
        Payload where = new Payload();
        where.addColumn("message_key", key);
        boolean contains = database.SQLSelect("localization", Arrays.asList(lang.name().toLowerCase()), where).size() > 0;

        if (contains)
            database.SQLUpdate("localization", newMessage, where);
        else {
            // else just insert
            newMessage.addColumn("message_key", key);
            database.SQLInsert("localization", newMessage);
        }
    }


    /**
     * Removes message from database
     *
     * @param key Key
     */
    public void removeMessage(@NotNull String key) {
        Payload where = new Payload();
        where.addColumn("message_key", key);

        database.SQLDelete("localization", where);

    }

}
