package me.MrWener.RealmLandEngine.components;

import com.sun.istack.internal.NotNull;
import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.AutonomousComponent;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import me.MrWener.RealmLandEngine.logging.Logger;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class AutonomousComponentEngine implements PluginComponent {

    private RealmLandEngine engine;
    private Logger logger;

    // Autonomous Components
    private List<Pair<AutonomousComponent, Boolean>> components = new ArrayList<>();
    private List<String> searchedIn = new ArrayList<>();

    public AutonomousComponentEngine(@NotNull RealmLandEngine engine) {
        this.engine = engine;
        this.logger = engine.logger();
    }

    @Override
    public void initialize() {
        findAutonomousComponents(engine);
        engine.getJuvenileEngine().getJuveniles().stream().forEach(realmLandJuvenile -> {
            if (realmLandJuvenile instanceof JavaPlugin) {
                findAutonomousComponents((JavaPlugin) realmLandJuvenile);
            }
        });

        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            try {
                component.initialize();
                components.set(i, Pair.of(component, true));
            } catch (Exception x) {
                components.set(i, Pair.of(component, false));
                logger.err("Failed to initialize component '" + component.getClass().getSimpleName() + "'", x);
            }
        }

    }

    @Override
    public void terminate() {
        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            Boolean initialized = components.get(i).getValue();
            if (initialized == null)
                initialized = false;

            if (initialized) {
                try {
                    component.terminate();
                    components.set(i, Pair.of(component, null));
                } catch (Exception x) {
                    components.set(i, Pair.of(component, false));
                    logger.err("Failed to terminate component '" + component.getClass().getSimpleName() + "'", x);
                }
            }
        }
    }

    /**
     * Registers autonomous component
     *
     * @param component
     */
    public void registerComponent(@NotNull AutonomousComponent component) {
        components.add(Pair.of(component, null));
    }

    /**
     * Finds and loads autonomous components
     *
     * @param p        Plugin
     * @param <Plugin> Type of Plugin
     */
    public <Plugin extends JavaPlugin> void findAutonomousComponents(Plugin p) {
        if (searchedIn.contains(p.getName())) {
            return;
        }

        try {
            searchedIn.add(p.getName());
            new JarFile(new File(p.getClass().getProtectionDomain().getCodeSource().getLocation().toURI())).stream()
                    .filter(entry -> !entry.isDirectory() && entry.getName().endsWith(".class"))
                    .map(ZipEntry::getName)
                    .map(entry -> entry.replaceAll("/", "."))
                    .map(entry -> entry.substring(0, entry.length() - ".class".length()))
                    .map(entry -> {
                        try {
                            return Class.forName(entry);
                        } catch (Throwable ignored) {
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .filter(clazz -> clazz.getSuperclass() != null)
                    .filter(clazz -> clazz.getSuperclass().equals(AutonomousComponent.class))
                    .map(clazz -> {
                        try {
                            return (AutonomousComponent) clazz.getConstructor(RealmLandEngine.class, JavaPlugin.class).newInstance(engine, p);
                        } catch (Throwable e) {
                            logger.err("Failed to load component '" + clazz.getSimpleName() + "'", e);
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .filter(component ->
                    {
                        AutonomousComponent component1 = getComponent(component.getClass().getSimpleName());
                        if (component1 != null) {
                            if (component1.getClass().getPackage().getName().equalsIgnoreCase(component.getClass().getPackage().getName())) {
                                logger.warn("Found ambiguous component '" + component.getClass().getSimpleName() + "' [" + component.getOwner() + ", " + component1.getOwner() + "]");
                                return false;
                            }
                        }
                        return true;
                    })
                    .forEach(this::registerComponent);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets component by name
     *
     * @param componentName
     * @return
     */
    public AutonomousComponent getComponent(@NotNull String componentName) {
        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            if (component.getClass().getSimpleName().equalsIgnoreCase(componentName)) {
                return component;
            }
        }
        return null;
    }

    /**
     * Checks if component is already registered
     *
     * @param componentName
     * @return
     */
    public boolean containsComponent(@NotNull String componentName) {
        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            if (component.getClass().getSimpleName().equalsIgnoreCase(componentName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Initializes component
     *
     * @param componentName Component Name
     * @return
     */
    public boolean initializeComponent(@NotNull String componentName) {
        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            if (component.getClass().getSimpleName().equalsIgnoreCase(componentName)) {
                try {
                    logger.warn("Initializing component: " + componentName);
                    component.initialize();
                    components.set(i, Pair.of(component, true));
                    return true;
                } catch (Exception x) {
                    components.set(i, Pair.of(component, false));
                    logger.err("Failed to initialize component '" + component.getClass().getSimpleName() + "'", x);
                }
            }
        }
        return false;
    }

    /**
     * Terminates component
     *
     * @param componentName Component Name
     * @return
     */
    public boolean terminateComponent(@NotNull String componentName) {
        for (int i = 0; i < components.size(); i++) {
            AutonomousComponent component = components.get(i).getKey();
            if (component.getClass().getSimpleName().equalsIgnoreCase(componentName)) {
                try {
                    logger.warn("Terminating component: " + componentName);
                    component.terminate();
                    components.set(i, Pair.of(component, null));
                    return true;
                } catch (Exception x) {
                    components.set(i, Pair.of(component, false));
                    logger.err("Failed to terminate component '" + component.getClass().getSimpleName() + "'", x);
                }

            }
        }
        return false;
    }

    public List<Pair<AutonomousComponent, Boolean>> getComponents() {
        return components;
    }
}
