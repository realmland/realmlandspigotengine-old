package me.MrWener.RealmLandEngine.localization.chat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TextComponentBuilder {

    private TextComponent component;

    /**
     * Creates empty component
     */
    public TextComponentBuilder() {
        component = new TextComponent();
    }

    /**
     * Creates component with text of {@param text}
     */
    public TextComponentBuilder(@NotNull String text) {
        component = new TextComponent(text);
    }

    /**
     * Creates component from {@param component}
     */
    public TextComponentBuilder(@NotNull TextComponent component) {
        this.component = component;
    }

    /**
     * Creates component with components of {@param components}
     * <p>After each component space will be appended</p>
     */
    public TextComponentBuilder(@NotNull List<TextComponent> components) {
        component = new TextComponent();

        // foreach those bad boys
        components.forEach(nextComponent -> {
            this.component.addExtra(nextComponent);
        });

    }

    /**
     * Creates component with components of {@param components}
     * <p>After each component space will be appended</p>
     */
    public TextComponentBuilder(@NotNull TextComponent... components) {
        this(Arrays.asList(components));
    }

    /**
     * Creates component with texts of {@param text}
     * <p>After each text space will be appended</p>
     */
    public TextComponentBuilder(@NotNull String... texts) {
        this(Arrays.stream(texts)
                .map(text -> new TextComponent(text))
                .collect(Collectors.toList()));
    }


    public TextComponentBuilder setExtra(@NotNull List<BaseComponent> components) {
        this.component.setExtra(components);
        return this;
    }

    public TextComponentBuilder addExtra(@NotNull String text) {
        this.component.addExtra(text);
        return this;
    }

    public TextComponentBuilder addExtra(@NotNull BaseComponent component) {
        this.component.addExtra(component);
        return this;
    }

    public TextComponentBuilder setColor(@NotNull ChatColor color) {
        this.component.setColor(color);

        return this;
    }

    public TextComponentBuilder setBold(@NotNull Boolean bold) {
        this.component.setBold(bold);

        return this;
    }

    public TextComponentBuilder setItalic(@NotNull Boolean italic) {
        this.component.setItalic(italic);

        return this;
    }

    public TextComponentBuilder setUnderlined(@NotNull Boolean underlined) {
        this.component.setUnderlined(underlined);

        return this;
    }

    public TextComponentBuilder setStrikethrough(@NotNull Boolean strikethrough) {
        this.component.setStrikethrough(strikethrough);

        return this;
    }

    public TextComponentBuilder setObfuscated(@NotNull Boolean obfuscated) {
        this.component.setObfuscated(obfuscated);

        return this;
    }

    public TextComponentBuilder setInsertion(@NotNull String insertion) {
        this.component.setInsertion(insertion);

        return this;
    }

    public TextComponentBuilder setClickEvent(@NotNull ClickEvent clickEvent) {
        this.component.setClickEvent(clickEvent);

        return this;
    }

    public TextComponentBuilder setHoverEvent(@NotNull HoverEvent hoverEvent) {
        this.component.setHoverEvent(hoverEvent);

        return this;
    }

    public @NotNull
    List<BaseComponent> getExtra() {
        return component.getExtra();
    }

    public @NotNull
    ClickEvent getClickEvent() {
        return component.getClickEvent();
    }

    public @NotNull
    HoverEvent getHoverEvent() {
        return component.getHoverEvent();
    }

    public @NotNull
    TextComponent getComponent() {
        return component;
    }

}
