package me.MrWener.RealmLandEngine;

import me.MrWener.RealmLandEngine.components.AutonomousComponentEngine;
import me.MrWener.RealmLandEngine.configurations.ConfigurationsHandler;
import me.MrWener.RealmLandEngine.database.DatabaseEngine;
import me.MrWener.RealmLandEngine.juvenile.JuvenileEngine;
import me.MrWener.RealmLandEngine.localization.Localization;
import me.MrWener.RealmLandEngine.logging.Logger;
import me.MrWener.RealmLandEngine.permissions.PermissionsHandler;
import me.MrWener.RealmLandEngine.ui.commands.CommandsEngine;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;


public class RealmLandEngine extends JavaPlugin implements Listener {

    private long start = 0;
    private long uptime;

    private boolean fuckedUp = false;

    private static RealmLandEngine instance;

    // Logger
    private Logger logger;

    // ConfigurationsHandler
    private ConfigurationsHandler configurationsHandler;

    // PermissionsHandler
    private PermissionsHandler permissionsHandler;

    // CommandsEngine
    private CommandsEngine commandsEngine;

    // Database
    private DatabaseEngine database;

    // Database
    private Localization localization;

    // Autonomous components
    private AutonomousComponentEngine autonomousComponentEngine;

    // Juveniles
    private JuvenileEngine juvenileEngine;


    @Override
    public void onLoad() {
        instance = this;

        start = System.currentTimeMillis();
        uptime = System.currentTimeMillis();

        try {
            logger = new Logger();
            logger.info("Logger constructed!");
        } catch (Throwable x) {
            logger.err("Failed to construct Logger", x);
        }

        try {
            configurationsHandler = new ConfigurationsHandler(this);
            logger.info("ConfigurationsHandler constructed");


        } catch (Throwable x) {
            logger.err("Failed to construct ConfigurationsHandler", x);
        }

        try {
            permissionsHandler = new PermissionsHandler(this);
            logger.info("PermissionsHandler constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct PermissionsHandler", x);
        }

        try {
            commandsEngine = new CommandsEngine(this);
            logger.info("CommandsEngine constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct CommandsEngine", x);
        }

        try {
            ConfigurationSection dbSection = getConfig().getConfigurationSection("database");

            @NotNull String host = "";
            @NotNull String databaseName = "";
            @NotNull String username = "";
            @NotNull String password = "";

            try {
                host = dbSection.getString("server");
            } catch (Throwable x) {
                logger.err("Failed to get 'database server' from config", x);
            }

            try {
                databaseName = dbSection.getString("database");
            } catch (Throwable x) {
                logger.err("Failed to get 'database name' from config", x);
            }

            String params = dbSection.getString("jdbc-url-params");
            int port = Integer.valueOf(dbSection.getString("port"));

            ConfigurationSection credentialsSection = getConfig().getConfigurationSection("database.credentials");
            try {
                username = credentialsSection.getString("username");
            } catch (Throwable x) {
                logger.err("Failed to get 'user name' from config", x);
            }

            try {
                password = credentialsSection.getString("password");
            } catch (Throwable x) {
                logger.err("Failed to get 'password' from config", x);
            }

            database = new DatabaseEngine(this, host, databaseName, username, password, port, params);

            DatabaseEngine.ConnectionSummary activeSum = database.testActiveConnection();
            logger.debug(activeSum.isSuccess() ? "§aActive connection to database SUCCESS" : "§cActive connection to database FAILED");

            DatabaseEngine.ConnectionSummary newSum = database.testNewConnection();
            logger.debug(newSum.isSuccess() ? "§aNew connection to database SUCCESS" : "§cNew connection to database FAILED");

            logger.info("DatabaseEngine constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct DatabaseEngine, plugin can not run without it!", x);
            fuckedUp = true;
            return;
        }

        try {
            localization = new Localization(this);
            logger.info("Localization constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct Localization", x);
        }


        try {
            autonomousComponentEngine = new AutonomousComponentEngine(this);
            logger.info("AutonomousComponentEngine constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct AutonomousComponentEngine", x);
        }

        try {
            juvenileEngine = new JuvenileEngine(this);
            logger.info("JuvenileEngine constructed");
        } catch (Throwable x) {
            logger.err("Failed to construct JuvenileEngine", x);
        }

    }

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        if (fuckedUp) {
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        // sem komponenty na initializovanie
        configurationsHandler.initialize();
        permissionsHandler.initialize();
        commandsEngine.initialize();
        localization.initialize();

        juvenileEngine.initialize();
        autonomousComponentEngine.initialize();

        logger.info("§aPlugin loaded!");
        logger.info("Plugin loaded & enabled in " + (System.currentTimeMillis() - start) + "ms!");
    }

    @Override
    public void onDisable() {
        // sem tiez nieco
        if (fuckedUp) {
            return;
        }

        autonomousComponentEngine.terminate();
        juvenileEngine.terminate();
        // Sem komponenty na terminovanie
        localization.terminate();
        commandsEngine.terminate();
        permissionsHandler.terminate();
        configurationsHandler.terminate();


        try {
            database.getActiveConnection().close();
            database.getSource().close();
        } catch (SQLException e) {
            logger.err("", e);
        }

        logger.info("§cPlugin disabled!");
        logger.info("Plugin uptime: " + (System.currentTimeMillis() - uptime) / 1000f + "s");
    }


    public static RealmLandEngine getEngine() {
        return instance;
    }

    public long getUptime() {
        return uptime;
    }

    public AutonomousComponentEngine getAutonomousComponentEngine() {
        return autonomousComponentEngine;
    }

    public JuvenileEngine getJuvenileEngine() {
        return juvenileEngine;
    }

    /**
     * @return Plugin's logger
     */
    public Logger logger() {
        return logger;
    }

    public PermissionsHandler getPermissionsHandler() {
        return permissionsHandler;
    }

    public CommandsEngine getCommandEngine() {
        return commandsEngine;
    }

    public ConfigurationsHandler getConfigurationsHandler() {
        return configurationsHandler;
    }

    public Localization getLocalization() {
        return localization;
    }

    public DatabaseEngine getDatabase() {
        return database;
    }
}
