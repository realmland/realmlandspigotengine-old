package me.MrWener.RealmLandEngine.ui.gui.custom;

import me.MrWener.RealmLandEngine.ui.gui.GUI;
import me.MrWener.RealmLandEngine.ui.gui.model.GUIComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MessageEditorGUI {

    private List<GUI> pages = new ArrayList<>();

    private final int pageMin = 10, pageMax = 43;
    private final int max = 28;

    public <Plugin extends JavaPlugin> TextComponent openEditor(@NotNull Plugin plugin, @NotNull Player player, @NotNull String[] rawMessages) {

        List<List<String>> pagesContent = getPages(rawMessages, new ArrayList<>());

        for (int i = 0; i < pagesContent.size(); i++) {
            pages.add(new GUI(plugin, "Message Editor §8//§r Page " + (++i), 54));
        }

        for (int pageIndex = 0; pageIndex < pages.size(); pageIndex++) {
            GUI page = pages.get(pageIndex);
            if (pages.get(pageIndex + 1) != null) {

            }
            List<String> pageContent = pagesContent.get(pageIndex);

            List<Integer> avaiableSlots = Arrays.asList(
                    10, 11, 12, 13, 14, 15, 16,
                    19, 20, 21, 22, 23, 24, 25,
                    28, 29, 30, 31, 32, 33, 34,
                    37, 38, 39, 40, 41, 42, 43);

            Iterator<Integer> slots = avaiableSlots.listIterator();
            pageContent.forEach(word -> {
                int slot = slots.next();
                page.addComponent(new GUIComponent(word, "wordNo" + slot, Material.GREEN_TERRACOTTA, () -> {
                    System.out.println(word);
                }), slot);
            });


            player.openInventory(pages.get(0).getInventory());
            return new TextComponent();
        }
        return null;
    }


    public List<List<String>> getPages(@NotNull String[] rawMessages, @NotNull List<List<String>> recur) {
        List<String> newLoad = new ArrayList<>();
        for (int i = 0; i < rawMessages.length; i++) {
            if (i == max) {
                recur.add(newLoad);
                return getPages(Arrays.copyOfRange(rawMessages, max, rawMessages.length), recur);
            }
            newLoad.add(rawMessages[i]);
        }
        recur.add(newLoad);
        return recur;
    }

}
