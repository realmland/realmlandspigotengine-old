package me.MrWener.RealmLandEngine.ui.commands.model;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.database.DatabaseEngine;
import me.MrWener.RealmLandEngine.localization.Localization;
import me.MrWener.RealmLandEngine.localization.chat.TextComponentBuilder;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Represents command
 */
public abstract class RealmLandCommand {

    protected RealmLandEngine instance;
    protected Localization localization;
    protected DatabaseEngine database;

    private Map<String, RealmLandSubCommand> subcommands = new HashMap<>();

    /**
     * Default constructor
     *
     * @param instance Instance of RealmLandEngine
     */
    public RealmLandCommand(@NotNull RealmLandEngine instance) {
        Validate.notNull(instance, "RealmLandEngine instance can not be null!");
        this.instance = instance;
        database = instance.getDatabase();
        localization = instance.getLocalization();
    }

    /**
     * Executes command
     *
     * @param sender Sender of command
     * @param label  Label of command
     * @param args   Arguments of command
     * @return true(success), false(failure)
     */
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
        TextComponent formatted = new TextComponent("Available sub-commands: \n  §7- ");
        if (subcommands.size() > 0) {
            Iterator<RealmLandSubCommand> subCommandListIterator = getSubCommands().values().iterator();
            while (subCommandListIterator.hasNext()) {
                RealmLandSubCommand sub = subCommandListIterator.next();
                if (!subCommandListIterator.hasNext()) {
                    formatted.addExtra(new TextComponentBuilder("§f" + sub.getName()).setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/engine " + sub.getName())).getComponent());
                } else {
                    formatted.addExtra(new TextComponentBuilder("§f" + sub.getName() + ", ").setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/engine " + sub.getName())).getComponent());
                }
            }
        } else {
            formatted = new TextComponentBuilder(Localization.PREFIX_INFO + "No sub-commands available.").getComponent();
        }
        sender.spigot().sendMessage(formatted);
        return true;
    }


    /**
     * Registers sub-command
     *
     * @param subCmd   Sub-command
     * @param <SubCmd> Type of RealmLandSubCommand
     */
    public <SubCmd extends RealmLandSubCommand> void registerSubCommand(@NotNull SubCmd subCmd) {
        if (subCmd.getName() == null)
            throw new IllegalArgumentException("Sub-Command name can not be null!");

        subcommands.put(subCmd.getName(), subCmd);
    }

    /**
     * Tries to find subcommand
     *
     * @param name SubCommand name
     * @return SubCommand
     */
    public RealmLandSubCommand getSubCommand(@NotNull String name) {
        return subcommands.get(name);
    }

    /**
     * @return List of command's sub-commands
     */
    public Map<String, RealmLandSubCommand> getSubCommands() {
        return subcommands;
    }

}
