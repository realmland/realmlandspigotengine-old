package me.MrWener.RealmLandEngine.configurations;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import me.MrWener.RealmLandEngine.configurations.model.ConfigurationModel;
import org.apache.commons.lang.Validate;
import org.bukkit.configuration.file.FileConfiguration;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationsHandler implements PluginComponent {

    private RealmLandEngine instance;

    private FileConfiguration pluginConfiguration;
    private final List<ConfigurationModel> configurations = new ArrayList<>();


    public ConfigurationsHandler(@NotNull RealmLandEngine instance) {
        this.instance = instance;

        this.pluginConfiguration = instance.getConfig();
        pluginConfiguration.options().copyDefaults(true);
        instance.saveDefaultConfig();
    }

    @Override
    public void initialize() {
        configurations.forEach(configurationModel -> configurationModel.load());
    }

    @Override
    public void terminate() {
        configurations.forEach(configurationModel -> configurationModel.save());
    }

    /**
     * Registers {@param configurationModel}
     *
     * @param configurationModel Config
     */
    public void registerConfiguration(@NotNull ConfigurationModel configurationModel) {
        Validate.notNull(configurationModel, "ConfigurationModel can not be null!");
        configurations.add(configurationModel);
    }

    /**
     * @return
     */
    public FileConfiguration getPluginConfiguration() {
        return pluginConfiguration;
    }
}

