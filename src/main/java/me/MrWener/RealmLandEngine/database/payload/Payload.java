package me.MrWener.RealmLandEngine.database.payload;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Payload {

    // final payload
    private final HashMap<String, Object> finalPayload = new HashMap<>();


    /**
     * Creates Payload object with one column
     *
     * @param column        Column name
     * @param value         Value
     * @param <PayloadType> Type of Payload
     * @return
     */
    public static <PayloadType extends Object> Payload of(@NotNull String column, @NotNull PayloadType value) {
        Payload p = new Payload();
        p.addColumn(column, value);
        return p;
    }

    /**
     * Merges two Payloads
     *
     * @param payload
     * @param payload2
     * @return
     */
    public static Payload merge(@NotNull Payload payload, @NotNull Payload payload2) {
        Payload updated = new Payload();
        payload.getFinalPayload().forEach((col, val) -> updated.forceAddColumn(col, val));
        payload2.getFinalPayload().forEach((col, val) -> updated.forceAddColumn(col, val));

        return updated;
    }

    /**
     * Adds column
     *
     * @param key       Column key
     * @param payload   Column payload
     * @param <Payload> Type of Payload
     * @return True(Success), False(Failure)
     */
    public <Payload extends Object> boolean addColumn(@NotNull String key, @NotNull Payload payload) {
        // must be unique
        if (finalPayload.containsKey(key))
            return false;

        forceAddColumn(key, payload);
        return true;
    }

    /**
     * Force Adds column, replaces old value
     *
     * @param key       Column key
     * @param payload   Column payload
     * @param <Payload> Type of Payload
     * @return True(Success), False(Failure)
     */
    public <Payload extends Object> boolean forceAddColumn(@NotNull String key, @NotNull Payload payload) {
        finalPayload.put(key, payload);
        return true;
    }

    /**
     * Sets column
     *
     * @param key       Column key
     * @param payload   Column payload
     * @param <Payload> Type of Payload
     * @return True(Success), False(Failure)
     */
    public <Payload extends Object> boolean setColumn(@NotNull String key, @NotNull Payload payload) {
        // don't need to check anything
        finalPayload.put(key, payload);
        return true;
    }

    /**
     * Removes column
     *
     * @param key Column key
     * @return True(Success), False(Failure)
     */
    public boolean removeColumn(@NotNull String key) {
        finalPayload.remove(key);
        return true;
    }

    /**
     * @param key Column key
     * @return Value
     */
    public <Payload extends Object> Payload getColumn(@NotNull String key) {
        return (Payload) finalPayload.get(key);
    }

    public List<String> getColumns() {
        return new ArrayList<>(getFinalPayload().keySet());
    }

    public List<Object> getValues() {
        return new ArrayList<>(getFinalPayload().values());
    }

    /**
     * @return Final payload
     */
    public HashMap<String, Object> getFinalPayload() {
        return finalPayload;
    }
}
