package me.MrWener.RealmLandEngine.configurations.model;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.logging.Logger;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class ConfigurationModel {
    private RealmLandEngine instance;

    private File dataFolder;

    private Logger logger;

    private String name;
    private File file;
    private FileConfiguration configuration;

    /**
     * Default constructor
     *
     * @param instance
     * @param name
     * @param copyDefault
     */
    public ConfigurationModel(@NotNull RealmLandEngine instance, @NotNull String name, boolean copyDefault) {
        this.instance = instance;
        this.logger = instance.logger();
        this.name = name;

        if(name.endsWith(".yml"))
            name = name.substring(0, name.length() - ".yml".length());

        dataFolder = instance.getDataFolder();
        file = new File(dataFolder, name + ".yml");

        if(!file.exists()) {
            if(copyDefault) {
                this.copyDefault();
                logger.debug("Copying default file for '" + name + "' configuration");
            } else {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    logger.err("", e);
                }
                logger.debug("Creating new file for '" + name + "' configuration");
            }
        }
        configuration = YamlConfiguration.loadConfiguration(file);
        load();
    }

    /**
     * Copies default file
     */
    public void copyDefault() {
        try {
            instance.saveResource(name + ".yml", true);
        } catch (IllegalArgumentException x) {
            logger.err("", x);
        }
    }

    public void save() {
        try {
            configuration.
                    save
                            (file);
        } catch (IllegalArgumentException x) {
            logger.err("", x);
        } catch (IOException x) {
            logger.err("", x);
        }
    }

    public void load() {
        try {
            configuration.load(file);
        } catch (IllegalArgumentException x) {
            logger.err("", x);
        } catch (IOException x) {
            logger.err("", x);
        } catch (InvalidConfigurationException x) {
            logger.err("", x);
        }
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        return file;
    }

    public FileConfiguration getConfiguration() {
        return configuration;
    }

    public File getDataFolder() {
        return dataFolder;
    }
}
