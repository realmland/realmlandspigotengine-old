package me.MrWener.RealmLandEngine.localization;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Language
 */
public enum Lang {
    SK, CZ, EN;

    /**
     * Checks if {@param name} is valid Language type
     *
     * @param name Language name
     * @return True || False
     */
    public static boolean validate(@NotNull String name) {
        if (Arrays.asList(values()).stream()
                .map(entry -> entry.name())
                .collect(Collectors.toList()).contains(name.toUpperCase()))
            return true;
        return false;
    }
}
