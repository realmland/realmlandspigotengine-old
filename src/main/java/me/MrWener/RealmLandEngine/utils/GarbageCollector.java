package me.MrWener.RealmLandEngine.utils;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.AutonomousComponent;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class GarbageCollector extends AutonomousComponent {

    private int taskID = 0;

    // ticks
    private int repeat = 120 * 20;

    public GarbageCollector(@NotNull RealmLandEngine instance, @NotNull JavaPlugin owner) {
        super(instance, owner);
    }

    @Override
    public void initialize() {
        try {
            repeat = instance.getConfigurationsHandler().getPluginConfiguration().getInt("utils.garbage-collector.repeat") * 20;
        } catch (Throwable x) {
            logger.err("Failed to get GC repeat time from config.", x);
        }
        logger.debug("GarbageCollector is running every " + repeat / 20 + " second/s");

        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, () ->
                        System.gc()
                , repeat, repeat);
    }

    @Override
    public void terminate() {
        Bukkit.getScheduler().cancelTask(taskID);
    }
}
