package me.MrWener.RealmLandEngine.database;

public enum ArrayType {

    VALUES('\''), COLUMNS('`');

    char character;

    ArrayType(char character) {
        this.character = character;
    }

    public char getCharacter() {
        return character;
    }
}

