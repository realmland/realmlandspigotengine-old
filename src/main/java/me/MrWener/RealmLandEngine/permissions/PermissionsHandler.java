package me.MrWener.RealmLandEngine.permissions;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.PluginComponent;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class PermissionsHandler implements PluginComponent {

    private RealmLandEngine instance;

    private String permissionMessage = "§9# §fSorry but you don't have permission do execute this action!";

    public PermissionsHandler(@NotNull RealmLandEngine instance) {
        this.instance = instance;
    }

    /**
     * Adds {@param permission} into permissions list
     *
     * @param permission Permission
     * @return true(success), false(failure)
     */
    public boolean registerPermission(@NotNull Permission permission) {
        // cant be null
        if (permission == null)
            return false;

        try {
            Bukkit.getPluginManager().addPermission(permission);
            return true;
        } catch (IllegalArgumentException x) {
            return false;
        }
    }

    /**
     * Adds {@param stringPermission} to the permission list
     *
     * @param stringPermission
     * @param permissionDefault
     * @return
     */
    public boolean registerPermission(@NotNull String stringPermission, @NotNull PermissionDefault permissionDefault) {
        return registerPermission(new Permission(stringPermission, permissionDefault));
    }

    /**
     * Finds permission by name
     *
     * @param stringPermission name of permission
     * @return permission is success, null if else
     */
    public Permission getPermission(@NotNull String stringPermission) {
        // cant be null
        if (stringPermission == null)
            return null;

        return Bukkit.getPluginManager().getPermission(stringPermission);
    }

    /**
     * Finds permission by name
     *
     * @param stringPermission name of permission
     * @return permission is success, null if else
     */
    public boolean permissionExists(@NotNull String stringPermission) {
        // cant be null
        if (stringPermission == null)
            return false;

        return Bukkit.getPluginManager().getPermission(stringPermission) != null;
    }

    @Override
    public void initialize() {
        registerPermission("realmland.admin", PermissionDefault.OP);
        registerPermission("realmland.player", PermissionDefault.TRUE);
    }

    @Override
    public void terminate() {

    }

    public String getPermissionMessage() {
        return permissionMessage;
    }

    public void setPermissionMessage(@NotNull String permissionMessage) {

    }
}
