package me.MrWener.RealmLandEngine.exceptions;

public class InvalidCommandException extends Throwable {

    public enum Type {
        MISSING_ANNOTATION("Command is missing CommandData annotation!"),
        MISSING_CONSTRUCTOR("Command is missing default command constructor, or it has other params");

        String message;

        Type(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private Type type;

    public InvalidCommandException(String command, Type type) {
        super("Invalid command '" + command + "'; " + type.getMessage());
        this.type = type;
    }

}
