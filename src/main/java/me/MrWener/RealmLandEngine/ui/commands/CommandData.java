package me.MrWener.RealmLandEngine.ui.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.CONSTRUCTOR)
public @interface CommandData {
    String name();

    String[] aliases() default {};
    String permission() default "realmland.player";
}
