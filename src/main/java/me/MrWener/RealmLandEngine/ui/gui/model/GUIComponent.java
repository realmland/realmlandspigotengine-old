package me.MrWener.RealmLandEngine.ui.gui.model;

import me.MrWener.RealmLandEngine.nbt.item.NBTItemEditor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GUIComponent {

    private String displayName;
    private String callName;
    private List<String> lore = new ArrayList<>();

    private int amount;
    private Material material;

    private ItemStack item;

    private Runnable runnable;


    /**
     * Creates GUIComponent with set parameters.
     *
     * @param displayName
     * @param callName
     * @param material
     * @param runnable
     */
    public GUIComponent(String displayName, String callName, Material material, Runnable runnable) {
        this(displayName, null, callName, material, runnable, 1);
    }

    /**
     * Creates GUIComponent with set parameters.
     *
     * @param displayName Display name of Component
     * @param callName    Call name of Component
     * @param material    Material of Component
     */
    public GUIComponent(@NotNull String displayName, @Nullable List<String> lore, @NotNull String callName, @NotNull Material material, @NotNull Runnable runnable) {
        this(displayName, lore, callName, material, runnable, 1);
    }

    /**
     * Creates GUIComponent with set parameters.
     *
     * @param displayName Display name of Component
     * @param callName    Call name of Component
     * @param material    Material of Component
     */
    public GUIComponent(@NotNull String displayName, @Nullable List<String> lore, @NotNull String callName, @NotNull Material material, @NotNull Runnable runnable, int amount) {
        this.displayName = displayName;
        this.callName = callName;
        this.amount = amount;
        this.material = material;
        this.runnable = runnable;

        if (lore != null)
            this.lore = lore;

        if (amount <= 0) {
            amount = 1;
        }
        if (amount > material.getMaxStackSize()) {
            amount = material.getMaxStackSize();
        }

        update();
    }

    private void update() {
        item = new ItemStack(material, amount);
        item = NBTItemEditor.writeNBT(item, "callName", callName);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.setLore(lore);
        item.setItemMeta(meta);
    }

    /**
     * Sets component DisplayName
     *
     * @param displayName
     */
    public GUIComponent setDisplayName(String displayName) {
        this.displayName = displayName;

        update();
        return this;
    }

    /**
     * Sets lore
     *
     * @param lore
     */
    public GUIComponent setLore(List<String> lore) {
        this.lore = lore;

        update();
        return this;
    }

    /**
     * Sets lore
     *
     * @param lore
     */
    public GUIComponent setLore(String... lore) {
        this.lore = Arrays.asList(lore);

        update();
        return this;
    }

    /**
     * Sets amount
     *
     * @param amount
     */
    public GUIComponent setAmount(int amount) {
        this.amount = amount;

        update();
        return this;
    }

    /**
     * Sets material
     *
     * @param material
     */
    public GUIComponent setMaterial(Material material) {
        this.material = material;

        update();
        return this;
    }

    /**
     * Executes task
     */
    public void exec() {
        runnable.run();
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getCallName() {
        return callName;
    }

    public int getAmount() {
        return amount;
    }

    public Material getMaterial() {
        return material;
    }

    public ItemStack getItem() {
        return item;
    }
}
