package me.MrWener.RealmLandEngine.ui.gui;

import me.MrWener.RealmLandEngine.nbt.item.NBTItemEditor;
import me.MrWener.RealmLandEngine.ui.gui.model.GUIComponent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

public class GUI implements Listener {
    private String displayName;

    private Inventory inventory;
    private int size = 9;

    protected TreeMap<Integer, GUIComponent> components = new TreeMap<>(new Comparator<Integer>() {
        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 > o2) {
                return 1;
            } else if (o2 > o1) {
                return -1;
            } else {
                return 0;
            }
        }
    });

    /**
     * Creates GUI
     *
     * @param displayName DisplayName of GUI
     * @param size        Size of GUI
     */
    public <Plugin extends JavaPlugin> GUI(@NotNull Plugin plugin, @NotNull String displayName, int size) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
        this.displayName = displayName;
        this.size = size;

        this.inventory = Bukkit.createInventory(null, size, displayName);
    }


    /**
     * Adds component into the GUI
     *
     * @param component Component
     * @param position  Position
     */
    public GUI addComponent(@NotNull GUIComponent component, int position) {
        validatePosition(position);

        components.put(position, component);
        return this;
    }

    /**
     * Removes component by position
     *
     * @param position Postition
     */
    public GUI removeComponent(int position) {
        validatePosition(position);

        components.remove(position);
        return this;
    }

    /**
     * Removes component by position and object
     *
     * @param component Component
     * @param position  Position
     */
    public GUI removeComponent(@NotNull GUIComponent component, int position) {
        validatePosition(position);

        components.remove(position, component);
        return this;
    }

    /**
     * Validates position
     *
     * @param position Position
     */
    public void validatePosition(int position) {
        if (position > size) {
            throw new IllegalArgumentException("Position of component can not be higher than size of GUI ( " + position + " !> " + size + ")");
        }
        if (position < 0) {
            throw new IllegalArgumentException("Ehhh... Position of component can not be lower than 0...");
        }
    }

    /**
     * @return Displayname of GUI
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return GUI Inventory
     */
    public Inventory getInventory() {
        components.forEach((pos, component) -> {
            inventory.setItem(pos, component.getItem());
        });

        return inventory;
    }

    /**
     * @return Size of GUI
     */
    public int getSize() {
        return size;
    }


    @EventHandler
    public void onClick(InventoryClickEvent e) {
        ItemStack item = e.getCurrentItem();
        if (item == null)
            item = e.getCursor();
        if (item == null)
            return;

        String callname = NBTItemEditor.getNBT(item, "callName");
        if (callname != null) {
            Iterator<GUIComponent> iterator = components.values().iterator();
            while (iterator.hasNext()) {
                GUIComponent component = iterator.next();
                if (component.getCallName().equalsIgnoreCase(callname)) {
                    e.setCancelled(true);
                    component.exec();
                }
            }
        }
    }
}
