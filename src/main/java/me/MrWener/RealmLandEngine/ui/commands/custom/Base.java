package me.MrWener.RealmLandEngine.ui.commands.custom;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.components.model.AutonomousComponent;
import me.MrWener.RealmLandEngine.juvenile.model.RealmLandJuvenile;
import me.MrWener.RealmLandEngine.localization.Lang;
import me.MrWener.RealmLandEngine.localization.Localization;
import me.MrWener.RealmLandEngine.localization.chat.TextComponentBuilder;
import me.MrWener.RealmLandEngine.ui.commands.CommandData;
import me.MrWener.RealmLandEngine.ui.commands.model.RealmLandCommand;
import me.MrWener.RealmLandEngine.ui.commands.model.RealmLandSubCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;


public class Base extends RealmLandCommand {


    @CommandData(name = "realmlandengine", aliases = {"base", "engine"}, permission = "realmland.admin")
    public Base(@NotNull RealmLandEngine instance) {
        super(instance);

        registerSubCommand(new LocalizationSubcommand(instance));
        registerSubCommand(new MemorySubCommand(instance));
        registerSubCommand(new ComponentsSubCommand(instance));
        registerSubCommand(new JuvenileSubCommand(instance));
    }

    @Override
    public boolean executeCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
        return super.executeCommand(sender, label, args);
    }


    private class LocalizationSubcommand extends RealmLandSubCommand {

        public LocalizationSubcommand(RealmLandEngine instance) {
            super(instance, "localization");

            registerAction(new Action(instance, "set") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    // require more arguments
                    if (!(args.length >= 3)) {
                        sender.spigot().sendMessage(
                                new TextComponentBuilder(Localization.PREFIX_ERR, "You must specify §okey, lang and message§r. §8Found " + args.length + "/3")
                                        .getComponent());
                        return true;
                    }

                    // action requirements
                    String key = args[0];

                    Lang lang = extractLang(args[1], sender);
                    if (lang == null) {
                        return true;
                    }

                    String message = "";

                    for (int i = 2; i < args.length; i++) {
                        message += args[i] + " ";
                    }
                    message = message.trim();

                    localization.setMessage(key, new TextComponent(message), lang);
                    sender.spigot().sendMessage(
                            new TextComponentBuilder(Localization.PREFIX_SUCCESS, "Done!")
                                    .getComponent());
                    return true;
                }
            });

            registerAction(new Action(instance, "get") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    // require more arguments
                    if (!(args.length >= 2)) {
                        sender.spigot().sendMessage(
                                new TextComponentBuilder(Localization.PREFIX_ERR, "You must specify §okey, lang and message§r. §8Found " + args.length + "/2")
                                        .getComponent());
                        return true;
                    }
                    // action requirements
                    String key = args[0];

                    Lang lang = extractLang(args[1], sender);
                    if (lang == null) {
                        return true;
                    }

                    sender.spigot().sendMessage(localization.getMessage(key, lang));
                    return true;
                }
            });

            registerAction(new Action(instance, "add") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    Map<String, String> req = require(sender, args, "key", "lang", "message");
                    if (req != null && req.size() > 2) {
                        String key = req.get("key");
                        Lang g = extractLang(req.get("lang"), sender);
                        if (g != null) {
                            String message = "";
                            for (int i = 2; i < args.length; i++) {
                                message += args[i];
                            }

                            boolean succ = localization.addMessage(key, new TextComponent(message), g);
                            sender.sendMessage(succ ? Localization.PREFIX_SUCCESS + "Done!" : Localization.PREFIX_ERR + "Failed!");
                        }
                    }
                    return true;
                }
            });

            registerAction(new Action(instance, "remove") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    Map<String, String> req = require(sender, args, "key");
                    if (req != null && req.size() > 0) {
                        String key = req.get("key");
                        Lang g = extractLang(req.get("lang"), sender);
                        if (g != null) {
                            String message = "";
                            for (int i = 2; i < args.length; i++) {
                                message += args[i];
                            }

                            boolean succ = localization.addMessage(key, new TextComponent(message), g);
                            sender.sendMessage(succ ? Localization.PREFIX_SUCCESS + "Done!" : Localization.PREFIX_ERR + "Failed!");
                        }
                    }
                    return true;
                }
            });

            registerAction(new Action(instance, "restart") {
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    localization.terminate();
                    localization.initialize();
                    sender.spigot().sendMessage(
                            new TextComponentBuilder(Localization.PREFIX_SUCCESS, "Localization restarted!")
                                    .getComponent());
                    return true;
                }
            });
        }

        protected Lang extractLang(@NotNull String langName, @NotNull CommandSender sender) {
            if (Lang.validate(langName.toUpperCase())) {
                return Lang.valueOf(langName.toUpperCase());
            } else {
                sender.spigot().sendMessage(
                        new TextComponentBuilder(Localization.PREFIX_ERR, "Undefined lang '" + langName.toUpperCase() + "' (" + Arrays.asList(Lang.values()) + "). ").getComponent());
                return null;
            }
        }
    }

    private class MemorySubCommand extends RealmLandSubCommand {
        public MemorySubCommand(RealmLandEngine instance) {
            super(instance, "memory");

            registerAction(new Action(instance, "gc") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    System.gc();
                    sender.sendMessage(Localization.PREFIX_SUCCESS + "Garbage run successful!");
                    return true;
                }
            });

            registerAction(new Action(instance, "runtime") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    sender.sendMessage("§7Free Memory: §a" + (Runtime.getRuntime().freeMemory() / 1e+6) + "mb");
                    sender.sendMessage("§7Max Memory: §a" + (Runtime.getRuntime().maxMemory() / 1e+6) + "mb");
                    sender.sendMessage("\n§f");
                    sender.sendMessage("§7Total Memory: §a" + (Runtime.getRuntime().totalMemory() / 1e+6) + "mb");
                    sender.sendMessage("§7Available Processors: §a" + Runtime.getRuntime().availableProcessors());
                    return true;
                }
            });
        }
    }

    private class ComponentsSubCommand extends RealmLandSubCommand {
        public ComponentsSubCommand(RealmLandEngine instance) {
            super(instance, "components");

            registerAction(new Action(instance, "intialize") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
                    String componentName = require(sender, args, "componentName").get("componentName");
                    if (componentName == null) {
                        return true;
                    }
                    boolean success = instance.getAutonomousComponentEngine().initializeComponent(componentName);
                    sender.spigot().sendMessage(success ?
                            new TextComponentBuilder(Localization.PREFIX_SUCCESS + "Successfully intialized component.").setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent("Component name: " + componentName)})).getComponent() :
                            new TextComponentBuilder(Localization.PREFIX_ERR + "Component does not exist OR something really bad happened.").setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent("Component name: " + componentName)})).getComponent());
                    return true;
                }
            });

            registerAction(new Action(instance, "terminate") {
                @Override
                public boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {

                    String componentName = require(sender, args, "componentName").get("componentName");
                    if (componentName == null) {
                        return true;
                    }
                    boolean success = instance.getAutonomousComponentEngine().terminateComponent(componentName);
                    sender.spigot().sendMessage(success ?
                            new TextComponentBuilder(Localization.PREFIX_SUCCESS + "Successfully terminated component.").setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent("Component name: " + componentName)})).getComponent() :
                            new TextComponentBuilder(Localization.PREFIX_ERR + "Component does not exist OR something really bad happened.").setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[]{new TextComponent("Component name: " + componentName)})).getComponent());
                    return true;
                }
            });
        }

        @Override
        public boolean executeSubCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
            ListIterator<Pair<AutonomousComponent, Boolean>> iterator = instance.getAutonomousComponentEngine().getComponents().listIterator();
            TextComponentBuilder builder = new TextComponentBuilder(Localization.PREFIX_INFO + "Registered components -> \n   §7[");

            while (iterator.hasNext()) {
                Pair<AutonomousComponent, Boolean> next = iterator.next();
                AutonomousComponent component = next.getKey();
                Boolean enabled = next.getValue();

                ChatColor color = ChatColor.GRAY;
                if (enabled != null)
                    color = enabled ? ChatColor.GREEN : ChatColor.RED;

                if (iterator.hasNext())
                    builder.addExtra(new TextComponentBuilder(color + component.getClass().getSimpleName()).addExtra("§7, ").getComponent());
                else
                    builder.addExtra(new TextComponentBuilder(color + component.getClass().getSimpleName()).getComponent());
            }

            sender.spigot().sendMessage(builder.addExtra("§7]").getComponent());
            return true;
        }
    }

    private class JuvenileSubCommand extends RealmLandSubCommand {

        public JuvenileSubCommand(RealmLandEngine instance) {
            super(instance, "juveniles");
        }

        @Override
        public boolean executeSubCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
            List<RealmLandJuvenile> juvenileList = instance.getJuvenileEngine().getJuveniles();
            TextComponent formated = new TextComponent(Localization.PREFIX_INFO + "Available juveniles -> \n  §7[");
            juvenileList.stream().forEach(juvenile -> {
                formated.addExtra(new TextComponentBuilder(juvenile.getClass().getSimpleName()).setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{
                        new TextComponentBuilder("§7Package: " + juvenile.getClass().getPackage()).getComponent(),
                        new TextComponentBuilder("§7Protection Domain" + juvenile.getClass().getProtectionDomain()).getComponent()
                })).getComponent());
            });
            formated.addExtra("§7]");
            sender.spigot().sendMessage(formated);
            return true;
        }
    }
}
