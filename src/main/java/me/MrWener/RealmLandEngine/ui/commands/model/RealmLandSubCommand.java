package me.MrWener.RealmLandEngine.ui.commands.model;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.database.DatabaseEngine;
import me.MrWener.RealmLandEngine.localization.Localization;
import me.MrWener.RealmLandEngine.localization.chat.TextComponentBuilder;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public abstract class RealmLandSubCommand {

    protected RealmLandEngine instance;
    protected Localization localization;
    protected DatabaseEngine database;

    private String name;

    protected List<Action> actions = new ArrayList<>();


    /**
     * Default constructor
     *
     * @param instance Instance of RealmLandEngine
     * @param name     Name of subcommand
     */
    public RealmLandSubCommand(RealmLandEngine instance, String name) {
        this.instance = instance;
        this.name = name;

        this.localization = instance.getLocalization();
        this.database = instance.getDatabase();
    }

    /**
     * Executes sub-command
     *
     * @param sender Sender of command
     * @param label  Label of command
     * @param args   Arguments of command
     * @return true(success), false(failure)
     */
    public boolean executeSubCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
        TextComponent formatted = new TextComponent();
        Iterator<Action> actionListIterator = getActions().iterator();
        while (actionListIterator.hasNext()) {
            String action = actionListIterator.next().name;
            if (!actionListIterator.hasNext()) {
                formatted.addExtra(new TextComponentBuilder("§f" + action).setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/engine " + getName() + " " + action)).getComponent());
            } else {
                formatted.addExtra(new TextComponentBuilder("§f" + action + ", ").setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/engine " + getName() + " " + action)).getComponent());
            }
        }

        sender.spigot().sendMessage(new TextComponentBuilder(new TextComponent(Localization.PREFIX_INFO), new TextComponent("Available actions: \n  §7- "), formatted).getComponent());
        return true;
    }


    /**
     * @return Actions of sub-command
     */
    public List<Action> getActions() {
        return actions;
    }


    /**
     * Checks if action with name of {@param name} is registered
     *
     * @param name Name of action
     * @return true/false
     */
    public boolean containsAction(@NotNull String name) {
        ListIterator<Action> actionListIterator = actions.listIterator();
        while (actionListIterator.hasNext()) {
            Action action = actionListIterator.next();
            if (action.getName().equalsIgnoreCase(name))
                return true;
        }
        return false;
    }

    /**
     * Gets registered action with name of {@param name}
     *
     * @param name Name of action
     * @return Action
     */
    public Action getAction(@NotNull String name) {
        if (containsAction(name)) {
            ListIterator<Action> actionListIterator = actions.listIterator();
            while (actionListIterator.hasNext()) {
                Action action = actionListIterator.next();
                if (action.getName().equalsIgnoreCase(name))
                    return action;
            }
        }
        return null;
    }

    /**
     * Registers action
     *
     * @param action Action
     */
    protected void registerAction(@NotNull Action action) {
        actions.add(action);
    }


    /**
     * @return Sub-Command name
     */
    public String getName() {
        return name;
    }


    /**
     * Represents RealmLandSubCommand action
     */
    public abstract class Action {

        protected RealmLandEngine instance;
        private String name;

        private List<String> requirements = new ArrayList<>();

        /**
         * Default constructor
         *
         * @param instance Instance of Engine
         * @param name     Name of Action
         */
        public Action(RealmLandEngine instance, String name) {
            this.instance = instance;
            this.name = name;
        }

        // todo MUSIA BYT DOPREDU ZAREGISTROVANE SUBCOMMANDY TY GENERERAT SKURVENY - MAROS/23:06

        public Map<String, String> require(@NotNull CommandSender sender, @NotNull String[] args, @NotNull String... requirements) {
            this.requirements = Arrays.asList(requirements);

            if (args.length < requirements.length) {
                sender.spigot().sendMessage(
                        new TextComponentBuilder(Localization.PREFIX_ERR, "You must specify §o" + Arrays.deepToString(requirements) + "§r. §8Found " + args.length + "/" + requirements.length)
                                .getComponent());
                return new HashMap<>();
            } else {
                Map<String, String> complete = new HashMap<>();
                for (int i = 0; i < requirements.length; i++) {
                    complete.put(requirements[i], args[i]);
                }
                return complete;
            }
        }

        public List<String> getRequirements() {
            return requirements;
        }

        public abstract boolean executeAction(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args);

        public String getName() {
            return name;
        }
    }
}
