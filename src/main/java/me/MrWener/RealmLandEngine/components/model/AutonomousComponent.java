package me.MrWener.RealmLandEngine.components.model;

import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.logging.Logger;
import org.apache.commons.lang.Validate;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

/**
 * Represents component that is no need to register
 */
public abstract class AutonomousComponent {

    protected RealmLandEngine instance;
    protected JavaPlugin owner;
    protected Logger logger;

    public AutonomousComponent(@NotNull RealmLandEngine instance, @NotNull JavaPlugin owner) {
        Validate.notNull(instance, "Instance can not be null");
        this.instance = instance;
        this.owner = owner;
        this.logger = instance.logger();
    }

    public abstract void initialize();

    public abstract void terminate();

    public JavaPlugin getOwner() {
        return owner;
    }
}
