package me.MrWener.RealmLandEngine.logging;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * Represents colorful logger
 */
public class Logger {

    private String PREFIX = "RealmLandEngine";
    private ChatColor PREFIX_COLOR = ChatColor.GOLD;

    private final String SEPARATOR = " > ";
    private final ChatColor SEPARATOR_COLOR = ChatColor.GRAY;

    private boolean enableDebug = true;
    private ChatColor DEBUG_COLOR = ChatColor.GRAY;

    private ChatColor INFO_COLOR = ChatColor.WHITE;
    private ChatColor WARN_COLOR = ChatColor.YELLOW;
    private ChatColor ERR_COLOR = ChatColor.RED;

    private static ConsoleCommandSender sender;

    private int warns = 0;
    private int errors = 0;

    static {
        sender = Bukkit.getConsoleSender();
    }

    public Logger() {
    }

    public Logger(@NotNull String PREFIX, @NotNull ChatColor PREFIX_COLOR) {
        this.PREFIX = PREFIX;
        this.PREFIX_COLOR = PREFIX_COLOR;
    }

    /**
     * Sends debug message to console.
     *
     * @param payload   Payload
     * @param <Payload> Type of Payload
     */
    public <Payload extends Object> void debug(@NotNull Payload payload) {
        if (enableDebug)
            send(payload.toString(), DEBUG_COLOR);
    }

    /**
     * Sends info message to console.
     *
     * @param payload   Payload
     * @param <Payload> Type of Payload
     */
    public <Payload extends Object> void info(@NotNull Payload payload) {
        send(payload.toString(), INFO_COLOR);
    }

    /**
     * Sends info message to console.
     *
     * @param payload   Payload
     * @param <Payload> Type of Payload
     */
    public <Payload extends Object> void warn(@NotNull Payload payload) {
        warns++;
        send(payload.toString(), WARN_COLOR);
    }

    /**
     * Sends error message to console.
     *
     * @param payload   Payload
     * @param <Payload> Type of Payload
     */
    public <Payload extends Object> void err(@NotNull Payload payload) {
        errors++;
        send(payload.toString(), ERR_COLOR);
    }

    /**
     * Sends errir message to console with Exception details
     *
     * @param payload   Payload
     * @param <Payload> Type of Payload
     */
    public <Payload extends Object, Excep extends Throwable> void err(@NotNull Payload payload, @NotNull Excep exception) {
        errors++;
        sender.sendMessage("§f");
        if(payload.toString().length() > 0)
            send(payload.toString(), ERR_COLOR);

        // print stacktrace if debug is enabled
        if(enableDebug) {
            sender.sendMessage(DEBUG_COLOR + "StackTrace elements: ");
            sender.sendMessage(DEBUG_COLOR + "Class: §f" + exception.getClass().getName());
            sender.sendMessage(DEBUG_COLOR + "Message: §f" + exception.getMessage());
            sender.sendMessage("§f");
            Arrays.asList(exception.getStackTrace()).forEach(elem -> System.err.println("| " + elem.toString()));
        }
        sender.sendMessage("§f");
    }


    private void send(@NotNull String payload, @NotNull ChatColor color) {
        Validate.notNull(payload, "Message payload for logger can not be null!");
        Validate.notNull(color, "Color payload for logger can not be null!");

        // If message is empty, just print new line
        if (payload.length() <= 0) {
            sender.sendMessage("§f");
            return;
        }

        sender.sendMessage(PREFIX_COLOR + PREFIX + SEPARATOR_COLOR + SEPARATOR + color + payload.trim());

    }

    /**
     * Enables debug messages
     */
    public void enableDebug() {
        enableDebug = true;
    }

    /**
     * Disables debug messages
     */
    public void disableDebug() {
        enableDebug = false;
    }

    /**
     * @return True if debug is enabled, False if else
     */
    public boolean isDebugEnabled() {
        return enableDebug;
    }

    public int getWarns() {
        return warns;
    }

    public int getErrors() {
        return errors;
    }
}
