package me.MrWener.RealmLandEngine.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import me.MrWener.RealmLandEngine.RealmLandEngine;
import me.MrWener.RealmLandEngine.database.payload.Payload;
import me.MrWener.RealmLandEngine.logging.Logger;
import org.apache.commons.lang.Validate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class DatabaseEngine {


    private RealmLandEngine kernel;
    private Logger logger;

    private HikariConfig config;
    private HikariDataSource source;

    private Connection acitiveConnection;

    /**
     * Default constructor
     *
     * @param kernel Instance of kernel
     */
    public DatabaseEngine(@NotNull RealmLandEngine kernel, @NotNull String host, @NotNull String database, @NotNull String username, @NotNull String password, int port, @NotNull String params) {
        this.kernel = kernel;
        this.logger = kernel.logger();

        config = new HikariConfig();
        config.setUsername(username);
        config.setPassword(password);
        config.setJdbcUrl(generateURL(host, database, port, params));
    }


    public void executeSQL(@NotNull String SQL) {
        try (Statement st = getActiveConnection().createStatement()) {
            st.execute(SQL);
        } catch (SQLException e) {
            logger.err("Failed to execute SQL: '" + SQL + "'.", e);
        }
    }

    /**
     * Inserts payload into table
     *
     * @param table   Table
     * @param payload Payload
     */
    public void SQLInsert(@NotNull String table, @NotNull Payload payload) {
        String sql = "INSERT into `" + table + "` (" + generateArray(payload.getColumns(), ArrayType.COLUMNS) + ") " +
                "VALUES (" + generateArray(payload.getValues(), ArrayType.VALUES) + ")";

        executeSQL(sql);
    }

    /**
     * Updates table values
     *
     * @param table   Table
     * @param updated Updated payload
     * @param where   Where paylaod
     */
    public void SQLUpdate(@NotNull String table, @NotNull Payload updated, @Nullable Payload where) {
        String whereSql = "";
        if (where != null)
            whereSql = " WHERE " + generateAssignArray(where.getFinalPayload());

        String sql = "UPDATE `" + table + "` SET " + generateAssignArray(updated.getFinalPayload()) + whereSql;

        executeSQL(sql);
    }

    public void SQLDelete(@NotNull String table, @NotNull Payload where) {
        String sql = "DELETE FROM `" + table + "` WHERE " + generateAssignArray(where.getFinalPayload());

        executeSQL(sql);
    }

    public void SQLInsertOrUpdate(@NotNull String table, @NotNull Payload updated, @Nullable Payload where) {
        if (SQLSelect(table, updated.getColumns(), where).size() > 0) {
            System.out.println("update");
            SQLUpdate(table, updated, where);
        } else {
            System.out.println("insert");
            SQLInsert(table, updated);
        }
    }


    /**
     * @param table
     * @param colums
     * @param where
     * @return
     */
    public List<Payload> SQLSelect(@NotNull String table, @NotNull List<String> colums, @Nullable Payload where) {
        List<Payload> payloads = new ArrayList<>();

        String whereSql = "";
        if (where != null)
            whereSql = " WHERE " + generateAssignArray(where.getFinalPayload()) + "";

        String sql = "SELECT " + generateArray(colums, ArrayType.COLUMNS) + " FROM `" + table + "`" + whereSql;

        try (Statement st = getActiveConnection().createStatement()) {
            ResultSet set = st.executeQuery(sql);

            if (set.isFirst()) {
                payloads.add(extract(set, colums));
            }
            while (set.next()) {
                payloads.add(extract(set, colums));
            }

        } catch (SQLException e) {
            logger.err("Failed to execute QUERY SQL: '" + sql + "'.", e);
        }


        return payloads;
    }

    private Payload extract(ResultSet set, List<String> colums) {
        Payload payload = new Payload();
        colums.forEach(col -> {
            try {
                if (set.getObject(col) != null) {
                    payload.addColumn(col, set.getObject(col));
                }
            } catch (SQLException e) {
                logger.err("Failed to retrieve object of column `" + col + "`", e);
            }
        });
        return payload;
    }

    /**
     * Tests active connection
     *
     * @return Summary of connection
     */
    public ConnectionSummary testActiveConnection() {
        return testConnection(getActiveConnection());
    }

    /**
     * Tests new connection
     *
     * @return Summary of connection
     */
    public ConnectionSummary testNewConnection() {
        try (Connection c = getNewConnection()) {
            return testConnection(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Tests connection
     *
     * @param connection Connection
     * @return Summary of connection
     */
    public ConnectionSummary testConnection(@NotNull Connection connection) {
        long start = System.currentTimeMillis();
        long total = 0;
        boolean success = true;

        Statement st = null;
        try {
            st = getNewConnection().createStatement();
            st.close();
            total = System.currentTimeMillis() - start;
        } catch (Throwable x) {
            logger.err("Caught exception while trying to test connection", x);
            success = false;
        } finally {
            try {
                return new ConnectionSummary(success, total, st.isClosed(), connection.isClosed());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Generates SQL Array of values/columns (ie. `uuid`, `test` or 'nope', 'noAgain')
     *
     * @param array Java array of columns/values
     * @param type  Columns/Values
     * @return SQL Array
     */
    public String generateArray(Collection<? extends Object> array, ArrayType type) {
        Validate.notEmpty(array, "Map can not be empty!");
        Validate.notNull(type);

        StringBuilder cols = new StringBuilder();
        Iterator<? extends Object> keys = array.iterator();
        while (keys.hasNext()) {
            Object key = keys.next();
            if (keys.hasNext()) {
                cols.append(type.getCharacter()
                        + key.toString() +
                        type.getCharacter() + ", ");
            } else {
                cols.append(type.getCharacter()
                        + key.toString() +
                        type.getCharacter());
            }
        }
        return cols.toString();
    }

    /**
     * Generates SQL Assign array (ie. `uuid` = 'nope', `test` = 'noAgain')
     *
     * @param map Java map
     * @return SQL Assign array
     */
    public String generateAssignArray(Map<String, Object> map) {
        Validate.notEmpty(map, "Map can not be empty!");

        StringBuilder builder = new StringBuilder();
        map.forEach((key, val) -> {
            builder.append("`" + key + "` = '" + val.toString() + "', ");
        });


        return builder.toString().substring(0, builder.toString().length() - ", ".length());
    }

    /**
     * Generates JDBC URL
     *
     * @param host     Server address (both IPv4 and DNS)
     * @param database Database name
     * @param port     Port of Database
     * @return JDBC URL String
     */
    public String generateURL(@NotNull String host, @NotNull String database, int port, @NotNull String params) {
        // can't be smaller than (or be) 0
        if (port <= 0)
            port = 3306;
        return "jdbc:mysql://" + host + ":" + port + "/" + database + (params.length() > 0 ? "?" + params : "");
    }

    /**
     * @return Hikari config
     */
    public HikariConfig getConfig() {
        return config;
    }

    /**
     * @return Hikari Data source
     */
    public HikariDataSource getSource() {
        return source;
    }


    /**
     * @return New connection
     */
    public Connection getNewConnection() {
        if (source == null || source.isClosed()) {
            source = new HikariDataSource(config);
        }

        try {
            return source.getConnection();
        } catch (SQLException e) {
            logger.err("Failed to return new connection", e);
        }
        return null;
    }

    /**
     * @return Active and maintained connection
     */
    public Connection getActiveConnection() {
        if (source == null || source.isClosed()) {
            source = new HikariDataSource(config);
        }
        try {
            if (acitiveConnection == null ||
                    acitiveConnection.isClosed()) {

                acitiveConnection =
                        getNewConnection();
            }
        } catch (SQLException e) {
            logger.err("Caught SQLException while trying to return active connection", e);
            return getNewConnection();
        }

        return acitiveConnection;
    }

    public class ConnectionSummary {

        private boolean success = false;

        private long totalTime;
        private boolean statementClosed = false;
        private boolean connectionClosed = false;

        public ConnectionSummary(boolean success, long totalTime, boolean statementClosed, boolean connectionClosed) {
            this.success = success;
            this.totalTime = totalTime;
            this.statementClosed = statementClosed;
            this.connectionClosed = connectionClosed;
        }

        public boolean isSuccess() {
            return success;
        }

        public long getTotalTime() {
            return totalTime;
        }

        public boolean isStatementClosed() {
            return statementClosed;
        }

        public boolean isConnectionClosed() {
            return connectionClosed;
        }

        @Override
        public String toString() {
            return "Success=" + success + ", TotalTime=" + totalTime + "ms, StatementClosed=" + statementClosed + ", ConnectionClosed=" + connectionClosed;
        }
    }
}
