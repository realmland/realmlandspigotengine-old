package me.MrWener.RealmLandEngine.ui.gui;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

/**
 * Represent open able GUI
 */
public interface Openable {

    /**
     * Opens GUI to specified Player
     *
     * @param player
     */
    <Plugin extends JavaPlugin> void openTo(@NotNull Player player, @NotNull Plugin plugin);

}
